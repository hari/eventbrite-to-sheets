<?php

/**
 * Autoload custom classes
 */

$folders = [
  __DIR__.'/src/api/',
  __DIR__.'/src/database/',
  __DIR__.'/src/models/'
];

spl_autoload_register(function ($name) {
  global $folders;
  foreach($folders as $folder) {
    if (file_exists($folder."$name.php")) {
      return require_once $folder."$name.php";
    }
  }
});