<?php

class AttendeeTable
{
    public static function create()
    {
      $sql = "CREATE TABLE `".ATTENDEE_TABLE_NAME."` ( 
              `row_id` INT NOT NULL AUTO_INCREMENT , 
              `first_name` VARCHAR(255) NOT NULL ,
              `last_name` VARCHAR(255) NOT NULL ,
              `quantity` INT NOT NULL,
              `email` VARCHAR(255) NOT NULL,
              `phone` VARCHAR(255) NULL DEFAULT NULL,
              `status` VARCHAR(255) NOT NULL ,
              `event_id` VARCHAR(255) NULL DEFAULT NULL, 
              `created_at` DATETIME NOT NULL,
              PRIMARY KEY (`row_id`));";
      return Connection::execute($sql);
    }

    public static function insertBulk($raw_sql) {
      $sql = sprintf('INSERT INTO `%s` (`first_name`, `last_name`, `email`, `phone`,
       `status`, `event_id`, `quantity`, `created_at`) VALUES %s', ATTENDEE_TABLE_NAME, $raw_sql);
      $stmt = Connection::get()->prepare($sql)->execute();
    }
}