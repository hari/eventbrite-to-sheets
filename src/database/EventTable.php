<?php

class EventTable
{
    public static function create()
    {
      $sql = "CREATE TABLE `".EVENT_TABLE_NAME."` ( 
              `row_id` INT NOT NULL AUTO_INCREMENT , 
              `name` VARCHAR(255) NOT NULL ,
              `event_id` VARCHAR(255) NULL DEFAULT NULL, 
              `created_at` DATETIME NOT NULL ,
              `changed_at` DATETIME NOT NULL ,
              `fetched_at` DATETIME NOT NULL ,
              `status` INT NOT NULL DEFAULT 0 ,
              PRIMARY KEY (`row_id`));";
      return Connection::execute($sql);
    }

    public static function insert($event)
    {
      if (!is_object($event)) {
        throw new Exception('data must be instance if models\Event');
      }
      $sql = sprintf('INSERT INTO `%s` (`name`, `event_id`, `created_at`, `changed_at`, `fetched_at`) 
              VALUES (:name, :eid, :cat, :chat, :fat )', EVENT_TABLE_NAME);
      $stm = Connection::get()->prepare($sql);
      $now = date('Y-m-d H:i:s');
      $stm->bindParam(':name', $data->name, PDO::PARAM_STR);
      $stm->bindParam(':eid', $data->event_id, PDO::PARAM_STR);
      $stm->bindParam(':cat', $data->created_at, PDO::PARAM_STR);
      $stm->bindParam(':chat', $data->changed_at, PDO::PARAM_INT);
      $stm->bindParam(':fat', $now, PDO::PARAM_INT);
      return $stm->execute();
    }

    public static function insertBulk($raw_sql)
    {
      $sql = sprintf('INSERT INTO `%s` (`name`, `event_id`, `created_at`, `changed_at`, `fetched_at`) VALUES %s', EVENT_TABLE_NAME, $raw_sql);
      $stmt = Connection::get()->prepare($sql)->execute();
    }

    public static function getById($id)
    {
      $sql = sprintf('SELECT * FROM `%s` WHERE `row_id` = :id', EVENT_TABLE_NAME);
      $stm = Connection::get()->prepare($sql);
      $stm->bindParam(':id', $name, PDO::PARAM_STR);
      if ($stm->execute()) {
        return $stm->fetch();
      }
      return null;
    }

    public static function getCount($column) {
      $sql = sprintf('SELECT COUNT(DISTINCT %s) FROM %s', $column, EVENT_TABLE_NAME);
      $stm = Connection::get()->prepare($sql);
      if ($stm->execute()) {
        $result = $stm->fetch();
        if (count($result) > 0) {
          return $result[array_keys($result)[0]];
        }
      }
      return 0;
    }

    public static function delete() {
      $sql = 'DELETE FROM '. EVENT_TABLE_NAME;
      return Connection::get()->prepare($sql)->execute();
    }

    public static function all()
    {
      $sql = sprintf('SELECT * FROM %s', EVENT_TABLE_NAME);
      $stmt = Connection::get()->prepare($sql);
      if ($stmt->execute()) {
        return $stmt->fetchAll();
      }
      return [];
    }

    /**
     * Gets next event of which the attendees are to be fetched
     *
     * @return void
     */
    public static function getNext()
    {
      $sql = sprintf("SELECT event_id, status FROM %s WHERE status <> '-1' LIMIT 1", EVENT_TABLE_NAME);
      $stmt = Connection::get()->prepare($sql);
      if ($stmt->execute()) {
        return $stmt->fetchAll();
      }
      return [];
    }

    public static function setPage($event_id, $page_no)
    {
      $sql = sprintf("UPDATE %s SET status = :page WHERE event_id = :eid", EVENT_TABLE_NAME);
      $stmt = Connection::get()->prepare($sql);
      $stmt->bindParam(':eid', $event_id, PDO::PARAM_STR);
      $stmt->bindParam(':page', $page_no, PDO::PARAM_INT);
      return $stmt->execute();
    }
}
