<?php

class BaseApiRequest
{
  private $_api_url;
  private $_headers = array();

  public function __construct($api_url, $default_headers)
  {
    $this->_api_url = $api_url;
    if (substr($this->_api_url, -1, 1) !== '/') {
      $this->_api_url .= '/';
    }
    $this->_headers = $default_headers;
  }

  protected function getUrl($path)
  {
    return $this->_api_url.$path;
  }

  public function get($path, $headers = array(), $options = array())
  {
    return Requests::get($this->getUrl($path), array_merge($this->_headers, $headers), array_merge(array('timeout' => 60), $options));
  }

  public function post($path, $data, $headers = array(), $options = array())
  {
    return Requests::post($this->getUrl($path), array_merge($this->_headers, $headers), $data, $options);
  }

  public function addHeader($key, $value)
  {
    $this->_headers[$key] = $value;
  }

  public function removeHeader($key)
  {
    if (!array_key_exists($key, $this->_headers)) {
      return false;
    }
    unset($this->_headers[$key]);
  }

  public function getHeaders()
  {
    return $this->_headers;
  }

  protected function buildQuery($params = array())
  {
    $query = '&';
    if (count($params) > 0) {
      $query = '?';
      foreach ($params as $key => $value) {
        $query .= "$key=$value&";
      }
    }
    //remove last &
    return substr($query, 0, strlen($query) - 1);
  }
  
}