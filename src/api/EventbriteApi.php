<?php

/**
 * A base class for both API to interface the requests.
 */
class EventbriteApi extends BaseApiRequest
{
    private $user_id = -1;

    private static $instance = null;

    public static function singleton()
    {
      if (self::$instance == null) {
        $default_headers = array(
          'Authorization' => 'Bearer '.$_ENV['EVENTBRITE_TOKEN']
        );
        self::$instance = new self($_ENV['EVENTBRITE_API_URL'], $default_headers);
        self::$instance->fetchUserId();
      }
      return self::$instance;
    }

    public function fetchUserId()
    {
      if ($this->user_id != -1) {
        return $this->user_id;
      }
      if (isset($_ENV['USER_ID']) && $_ENV['USER_ID'] != -1) {
        return $this->user_id = $_ENV['USER_ID'];
      }
      $response = $this->get('users/me/');
      $response = json_decode($response->body);
      if (property_exists($response, 'id')) {
        SettingTable::put('USER_ID', $response->id);
        return $this->user_id = $response->id;
      }
      return -1;
    }

    public function getEvents()
    {
      $params = array('user.id' => $this->user_id);
      $page = $_ENV['EVENT_PAGE_NO'];
      if ($page != 1) {
        $params['page'] = $page;
      }
      $response = $this->get('events/search', $params);
      $events = json_decode($response->body);
      if (!is_object($events) || !property_exists($events, 'events')) {
        return array();
      }
      if ($events->pagination->page_number == $events->pagination->page_count) {
        SettingTable::put('EVENT_PAGE_NO', 0);
      } else {
        SettingTable::put('EVENT_PAGE_NO', $events->pagination->page_number + 1);
      }
      return $events->events;
    }

    public function getAttendee($event_id, $page_no)
    {
      $params = array();
      if ($page_no != 0) {
        $params['page'] = $page_no;
      }
      $response = $this->get("/events/$event_id/attendees/", $params);
      $atts = json_decode($response->body);
      if (!property_exists($atts, 'attendees')) {
        return array();
      }
      if ($atts->pagination->page_number == $atts->pagination->page_count) {
        EventTable::setPage($event_id, -1);
      } else {
        EventTable::setPage($event_id, $atts->pagination->page_number + 1);
      }
      return $atts->attendees;
    }
}
