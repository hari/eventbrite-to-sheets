<?php

class Utils
{
  public static function fetchEvent()
  {
    
  }

  public static function saveUserId()
  {
    $api = EventbriteApi::singleton();
    SettingTable::put('USER_ID', $api->fetchUserId());
  }

  public static function generateForm($env = array(), $needed_config = array())
  {
    $html = '';
    $input_template = '<label>%s</label><input name="%s" placeholder="%s" type="text" required />';
    foreach ($needed_config as $config => $placeholder) {
      if (substr($config, 0, 5) == 'title') {
        $html .= "<h3>$placeholder</h3>";
      } elseif (!array_key_exists($config, $env)) {
        if (stristr($config, '_pass')) {
          $html .= sprintf(str_replace('required', '', $input_template), str_replace('_', ' ', $config), $config, $placeholder);
        } else {
          $html .= sprintf($input_template, str_replace('_', ' ', $config), $config, $placeholder);
        }
      }
    }
    return '<form class="guide" action="" method="POST">'.$html.'<button type="submit" class="primary-button u-full-width">Save</button></form>';
  }
  
  public static function loadConfig()
  {
    $sql = 'SELECT * FROM '.TABLE_NAME;
    $rows = Connection::fetch($sql);
    $temp = array();
    foreach ($rows as $row) {
      $temp[$row['name']] = $row['value'];
    }
    return $temp;
  }

  public static function findEmptyKey($arr = array())
  {
    $empty_fields = array();
    foreach ($arr as $key => $value) {
      //ignore db_pass
      if (stristr($key, '_pass')) {
        continue;
      }
      if (null === $value || trim($value) === "") {
        $empty_fields[] = str_replace('_', ' ', $key);
      }
    }
    return $empty_fields;
  }
  
  /**
   * Creates src/db_config.php file by reading the data from POST request
   *
   * @return void
   */
  public static function createConfig()
  {
    $define_syntax = "define('%s', '%s');";
    $config = array();
    foreach ($_POST as $key => $value) {
      $config[] = sprintf($define_syntax, $key, $value);
    }
    $config[] = sprintf($define_syntax, 'TABLE_NAME', 'ebs_setting');
    $config[] = sprintf($define_syntax, 'EVENT_TABLE_NAME', 'ebs_event');
    $config[] = sprintf($define_syntax, 'ATTENDEE_TABLE_NAME', 'ebs_attendee');
    file_put_contents('src/db_config.php', "<?php\r\n".implode("\r\n", $config));
  }

  public static function saveSetting($data = null)
  {
    if ($data == null) {
      $data = $_POST;
    }
    foreach ($data as $key => $value) {
      SettingTable::put($key, $value);
    }
  }

  public static function storeNextEvents()
  {
    $page = $_ENV['EVENT_PAGE_NO'];
    if ($page == 0) {
      return;
    }
    $api = EventbriteApi::singleton();
    $events = $api->getEvents();
    $now = date('Y-m-d H:i:s');
    $raw_sql = array();
    foreach($events as $event) {
      $objEvent = new Event();
      $objEvent->id = $event->id;
      $objEvent->name = $event->name->text;
      $objEvent->created_at = date('Y-m-d H:i:s', strtotime($event->created));
      $objEvent->changed_at = date('Y-m-d H:i:s', strtotime($event->changed));
      $objEvent->fetched_at = $now;
      $raw_sql[] = sprintf("('%s', '%s', '%s', '%s', '%s')", $objEvent->name, $objEvent->id,
                       $objEvent->created_at, $objEvent->changed_at, $now);
    }
    EventTable::insertBulk(implode(',', $raw_sql));
  }
  
  /**
   * Gets next event and fetches its employee.
   * Sets -1 to status if event's attendees are all fetched else next page number
   * default status is 0 i.e it has not been fetched
   * @return void
   */
  public static function fetchAttendee()
  {
    $event = EventTable::getNext();
    if (empty($event)) {
      return array();
    }
    $event = $event[0];
    $api = EventbriteApi::singleton();
    $attendees = $api->getAttendee($event['event_id'], $event['status']);
    $raw_sql = [];
    foreach($attendees as $attendee) {
      $att = new Attendee();
      $att->first_name = $attendee->profile->first_name;
      $att->last_name = $attendee->profile->last_name;
      $att->email = $attendee->profile->email;
      if (property_exists($attendee->profile, 'cell_phone')) {
        $att->phone = $attendee->profile->cell_phone;
      } else {
        $att->phone = '00000000';
      }
      $att->status = $attendee->status;
      $att->quantity = $attendee->quantity;
      $att->event_id = $attendee->event_id;
      $att->created_at = date('Y-m-d H:i:s', strtotime($attendee->created));
      $raw_sql[] = sprintf("('%s', '%s', '%s', '%s', '%s', '%s', %d, '%s')", $att->first_name
               , $att->last_name, $att->email, $att->phone, $att->status
               , $att->event_id, $att->quantity, $att->created_at);
    }
    AttendeeTable::insertBulk(implode(',', $raw_sql));
  }
}